<?php


namespace Ipol\Viadelivery\Api\Encoder;


interface EncoderInterface
{
    public function encodeToAPI($handle);

    public function encodeFromAPI($handle);
}