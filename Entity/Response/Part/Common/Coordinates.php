<?php


namespace Ipol\Viadelivery\Api\Entity\Response\Part\Common;


use Ipol\Viadelivery\Api\Entity\Response\Part\AbstractResponsePart;

class Coordinates extends \Ipol\Viadelivery\Api\Entity\Universal\Coordinates
{
    use AbstractResponsePart;
}