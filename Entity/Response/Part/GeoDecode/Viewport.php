<?php


namespace Ipol\Viadelivery\Api\Entity\Response\Part\GeoDecode;


use Ipol\Viadelivery\Api\Entity\AbstractEntity;
use Ipol\Viadelivery\Api\Entity\Response\Part\AbstractResponsePart;
use Ipol\Viadelivery\Api\Entity\Response\Part\Common\Coordinates;

class Viewport extends AbstractEntity
{
    use AbstractResponsePart;

    /**
     * @var Coordinates
     */
    protected $northeast;
    /**
     * @var Coordinates
     */
    protected $southwest;

    /**
     * @return Coordinates
     */
    public function getNortheast(): Coordinates
    {
        return $this->northeast;
    }

    /**
     * @param Coordinates $arNortheast
     */
    public function setNortheast(Coordinates $arNortheast): void
    {
        $this->northeast = new Coordinates($arNortheast);
    }

    /**
     * @return Coordinates
     */
    public function getSouthwest(): Coordinates
    {
        return $this->southwest;
    }

    /**
     * @param array $arSouthwest
     */
    public function setSouthwest(array $arSouthwest): void
    {
        $this->southwest = new Coordinates($arSouthwest);
    }

}