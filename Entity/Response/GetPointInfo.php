<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


class GetPointInfo extends AbstractResponse
{
    /**
     * @var string(36) "82fd763e-fae5-4e83-bf34-935ea186f749"
     */
    protected $id;
    /**
     * @var string|null "X5"
     */
    protected $partner;
    /**
     * @var string|null "POSTAMAT"
     */
    protected $type;
    /**
     * @var float|null (55.82414)
     */
    protected $lat;
    /**
     * @var float|null (37.626561)
     */
    protected $lng;
    /**
     * @var string|null "Postamat in local shop"
     */
    protected $description;
    /**
     * @var string|null "08:00:00"
     */
    protected $working_time_from;
    /**
     * @var string|null "23:00:00"
     */
    protected $working_time_to;
    /**
     * @var string|null "129515"
     */
    protected $zip_code;
    /**
     * @var string|null "Tverskaya st., 33 A"
     */
    protected $street;
    /**
     * @var string|null "33 A"
     */
    protected $building;
    /**
     * @var string|null "Moscow"
     */
    protected $city;
    /**
     * @var string|null "city"
     */
    protected $city_pref;
    /**
     * @var string|null ""
     */
    protected $metro;
    /**
     * @var string|null "Moscow"
     */
    protected $region;
    /**
     * @var string|null(2) "RU"
     */
    protected $country;
    /**
     * @var string|null
     */
    protected $zone;
    /**
     * @var string|null "Moscow city, Tverskaya st., 33 A"
     */
    protected $full_address;
    /**
     * @var int|null
     */
    protected $retention_time;
    /**
     * @var string|null(1) "N"
     */
    protected $return_allowed;
    /**
     * @var string|null "ACTIVE"
     */
    protected $status;
    /**
     * @var string|null(1) "N"
     */
    protected $cash_allowed;
    /**
     * @var string|null(1) "N"
     */
    protected $card_allowed;
    /**
     * @var string|null(1) "Y"
     */
    protected $online_allowed;
    /**
     * @var string|null(1) "Y"
     */
    protected $prepayment_allowed;
    /**
     * @var string|null "129.00"
     */
    protected $price;
    /**
     * @var string|null(3) "RUB"
     */
    protected $currency;
    /**
     * @var int|null
     */
    protected $delivery_time;
    /**
     * @var int|null
     */
    protected $min_days;
    /**
     * @var int|null
     */
    protected $max_days;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return GetPointInfo
     */
    public function setId(string $id): GetPointInfo
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPartner(): ?string
    {
        return $this->partner;
    }

    /**
     * @param string|null $partner
     * @return GetPointInfo
     */
    public function setPartner(?string $partner): GetPointInfo
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return GetPointInfo
     */
    public function setType(?string $type): GetPointInfo
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     * @return GetPointInfo
     */
    public function setLat(?float $lat): GetPointInfo
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLng(): ?float
    {
        return $this->lng;
    }

    /**
     * @param float|null $lng
     * @return GetPointInfo
     */
    public function setLng(?float $lng): GetPointInfo
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return GetPointInfo
     */
    public function setDescription(?string $description): GetPointInfo
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWorkingTimeFrom(): ?string
    {
        return $this->working_time_from;
    }

    /**
     * @param string|null $working_time_from
     * @return GetPointInfo
     */
    public function setWorkingTimeFrom(?string $working_time_from): GetPointInfo
    {
        $this->working_time_from = $working_time_from;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWorkingTimeTo(): ?string
    {
        return $this->working_time_to;
    }

    /**
     * @param string|null $working_time_to
     * @return GetPointInfo
     */
    public function setWorkingTimeTo(?string $working_time_to): GetPointInfo
    {
        $this->working_time_to = $working_time_to;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    /**
     * @param string|null $zip_code
     * @return GetPointInfo
     */
    public function setZipCode(?string $zip_code): GetPointInfo
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return GetPointInfo
     */
    public function setStreet(?string $street): GetPointInfo
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBuilding(): ?string
    {
        return $this->building;
    }

    /**
     * @param string|null $building
     * @return GetPointInfo
     */
    public function setBuilding(?string $building): GetPointInfo
    {
        $this->building = $building;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return GetPointInfo
     */
    public function setCity(?string $city): GetPointInfo
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCityPref(): ?string
    {
        return $this->city_pref;
    }

    /**
     * @param string|null $city_pref
     * @return GetPointInfo
     */
    public function setCityPref(?string $city_pref): GetPointInfo
    {
        $this->city_pref = $city_pref;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetro(): ?string
    {
        return $this->metro;
    }

    /**
     * @param string|null $metro
     * @return GetPointInfo
     */
    public function setMetro(?string $metro): GetPointInfo
    {
        $this->metro = $metro;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     * @return GetPointInfo
     */
    public function setRegion(?string $region): GetPointInfo
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return GetPointInfo
     */
    public function setCountry(?string $country): GetPointInfo
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getZone(): ?string
    {
        return $this->zone;
    }

    /**
     * @param string|null $zone
     * @return GetPointInfo
     */
    public function setZone(?string $zone): GetPointInfo
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFullAddress(): ?string
    {
        return $this->full_address;
    }

    /**
     * @param string|null $full_address
     * @return GetPointInfo
     */
    public function setFullAddress(?string $full_address): GetPointInfo
    {
        $this->full_address = $full_address;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRetentionTime(): ?int
    {
        return $this->retention_time;
    }

    /**
     * @param int|null $retention_time
     * @return GetPointInfo
     */
    public function setRetentionTime(?int $retention_time): GetPointInfo
    {
        $this->retention_time = $retention_time;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReturnAllowed(): ?string
    {
        return $this->return_allowed;
    }

    /**
     * @param string|null $return_allowed
     * @return GetPointInfo
     */
    public function setReturnAllowed(?string $return_allowed): GetPointInfo
    {
        $this->return_allowed = $return_allowed;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return GetPointInfo
     */
    public function setStatus(?string $status): GetPointInfo
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCashAllowed(): ?string
    {
        return $this->cash_allowed;
    }

    /**
     * @param string|null $cash_allowed
     * @return GetPointInfo
     */
    public function setCashAllowed(?string $cash_allowed): GetPointInfo
    {
        $this->cash_allowed = $cash_allowed;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCardAllowed(): ?string
    {
        return $this->card_allowed;
    }

    /**
     * @param string|null $card_allowed
     * @return GetPointInfo
     */
    public function setCardAllowed(?string $card_allowed): GetPointInfo
    {
        $this->card_allowed = $card_allowed;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOnlineAllowed(): ?string
    {
        return $this->online_allowed;
    }

    /**
     * @param string|null $online_allowed
     * @return GetPointInfo
     */
    public function setOnlineAllowed(?string $online_allowed): GetPointInfo
    {
        $this->online_allowed = $online_allowed;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrepaymentAllowed(): ?string
    {
        return $this->prepayment_allowed;
    }

    /**
     * @param string|null $prepayment_allowed
     * @return GetPointInfo
     */
    public function setPrepaymentAllowed(?string $prepayment_allowed): GetPointInfo
    {
        $this->prepayment_allowed = $prepayment_allowed;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @param string|null $price
     * @return GetPointInfo
     */
    public function setPrice(?string $price): GetPointInfo
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     * @return GetPointInfo
     */
    public function setCurrency(?string $currency): GetPointInfo
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDeliveryTime(): ?int
    {
        return $this->delivery_time;
    }

    /**
     * @param int|null $delivery_time
     * @return GetPointInfo
     */
    public function setDeliveryTime(?int $delivery_time): GetPointInfo
    {
        $this->delivery_time = $delivery_time;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinDays(): ?int
    {
        return $this->min_days;
    }

    /**
     * @param int|null $min_days
     * @return GetPointInfo
     */
    public function setMinDays(?int $min_days): GetPointInfo
    {
        $this->min_days = $min_days;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxDays(): ?int
    {
        return $this->max_days;
    }

    /**
     * @param int|null $max_days
     * @return GetPointInfo
     */
    public function setMaxDays(?int $max_days): GetPointInfo
    {
        $this->max_days = $max_days;

        return $this;
    }

}