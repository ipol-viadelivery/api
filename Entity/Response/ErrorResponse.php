<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


class ErrorResponse extends AbstractResponse
{
    public function __construct()
    {
        parent::__construct(json_encode(['empty']));
    }
}