<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


use Ipol\Viadelivery\Api\Entity\Response\Part\GeoDecode\Viewport;

class GeoDecode extends AbstractResponse
{
    /**
     * @var string|null - "West Menlo Park, CA 94025, USA"
     */
    protected $formatted_address;
    /**
     * @var string|null - country code like "US"
     */
    protected $country;
    /**
     * @var string|null - region (for USA region code like "CA")
     */
    protected $region;
    /**
     * @var string|null
     */
    protected $city;
    /**
     * @var string|null
     */
    protected $zip_code;
    /**
     * @var string|null
     */
    protected $street;
    /**
     * @var string|null
     */
    protected $building;
    /**
     * @var float|null
     */
    protected $lat;
    /**
     * @var float|null
     */
    protected $lng;
    /**
     * @var Viewport|null
     */
    protected $viewport;
    /**
     * @var string (can be empty string)
     */
    protected $source_data;

    /**
     * @return string
     */
    public function getFormattedAddress(): ?string
    {
        return $this->formatted_address;
    }

    /**
     * @param string $formatted_address
     */
    public function setFormattedAddress(string $formatted_address): void
    {
        $this->formatted_address = $formatted_address;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     */
    public function setRegion(?string $region): void
    {
        $this->region = $region;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    /**
     * @param string|null $zip_code
     */
    public function setZipCode(?string $zip_code): void
    {
        $this->zip_code = $zip_code;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     */
    public function setStreet(?string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getBuilding(): ?string
    {
        return $this->building;
    }

    /**
     * @param string|null $building
     */
    public function setBuilding(?string $building): void
    {
        $this->building = $building;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     */
    public function setLat(?float $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return float|null
     */
    public function getLng(): ?float
    {
        return $this->lng;
    }

    /**
     * @param float|null $lng
     */
    public function setLng(?float $lng): void
    {
        $this->lng = $lng;
    }

    /**
     * @return Viewport|null
     */
    public function getViewport(): ?Viewport
    {
        return $this->viewport;
    }

    /**
     * @param array|null $arViewport
     */
    public function setViewport(?array $arViewport): void
    {
        if($arViewport) {
            $this->viewport = new Viewport($arViewport);
        }
    }

    /**
     * @return string
     */
    public function getSourceData(): string
    {
        return $this->source_data;
    }

    /**
     * @param string $source_data
     */
    public function setSourceData(string $source_data): void
    {
        $this->source_data = $source_data;
    }
}