<?php

namespace Ipol\Viadelivery\Api\Entity\Response;

use Ipol\Viadelivery\Api\BadResponseException;
use Ipol\Viadelivery\Api\Entity\AbstractEntity;

class AbstractResponse extends AbstractEntity
{
    /**
     * @var string
     */
    protected $origin;

    /**
     * @var mixed
     */
    protected $decoded;
    /**
     * @var null|string
     */
    protected $error;
    /**
     * @var bool
     */
    protected $Success;

    /**
     * @throws BadResponseException
     */
    function __construct($json)
    {
        parent::__construct();

        $this->origin = $json;

        if (empty($json)) {
            throw new BadResponseException('Empty server answer ' . __CLASS__);
        }

        $this->setDecoded(json_decode($json));

        if (is_null($this->decoded)) {
            throw new BadResponseException('Incorrect server answer ' . __CLASS__);
        }
    }

    /**
     * @return mixed
     */
    public function getDecoded()
    {
        return $this->decoded;
    }

    /**
     * @param mixed $decoded
     * @return $this
     */
    public function setDecoded($decoded)
    {
        $this->decoded = $decoded;
        return $this;
    }

    /**
     * @return bool
     */
    public function getSuccess(): bool
    {
        return $this->Success;
    }

    /**
     * @param bool $Success
     * @return $this
     */
    public function setSuccess(bool $Success)
    {
        $this->Success = $Success;
        return $this;
    }

}