<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


use Ipol\Viadelivery\Api\BadResponseException;
use Ipol\Viadelivery\Api\Entity\Response\Part\GetDeliveryInfoModeDistance\PointList;

/**
 * Class GetDeliveryInfoModeDistance
 * @package Ipol\Viadelivery\Api
 * @subpackage Response
 */
class GetDeliveryInfoModeDistance extends AbstractResponse
{
    /**
     * @var PointList
     */
    protected $pointArray;

    /**
     * @return PointList
     */
    public function getPointArray(): PointList
    {
        return $this->pointArray;
    }

    /**
     * @param array $pointArray
     * @throws BadResponseException
     */
    public function setPointArray(array $pointArray): void
    {
        $collection = new PointList();
        $this->pointArray = $collection->fillFromArray($pointArray);
    }

    public function setFields($fields)
    {
        return parent::setFields(['pointArray' => $fields]);
    }

}