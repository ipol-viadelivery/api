<?php


namespace Ipol\Viadelivery\Api\Entity\Request;


/**
 * Class GetDeliveryInfo
 * @package Ipol\Viadelivery\Api
 * @subpackage Request
 */
class GetDeliveryInfoModePrice extends AbstractRequest
{
    /**
     * @var string = price - mode for closest points
     */
    protected $group = 'price';
    /**
     * @var string - shop UUID - documentations marks this optional, but really now its required
     */
    protected $id = 'default';
    /**
     * @var int|null gram
     */
    protected $weight;
    /**
     * @var float|null cm
     */
    protected $length;
    /**
     * @var float|null cm
     */
    protected $width;
    /**
     * @var float|null cm
     */
    protected $height;
    /**
     * @var float|null
     */
    protected $order_price;
    /**
     * @var string|null - POSTAMAT|TOBACCO|RESTAURANT|POINT
     */
    protected $type;
    /**
     * @var string|null - X5|Sberlogistics
     */
    protected $partner;
    /**
     * @var string|null - optional city of departure
     */
    protected $fromCity;
    /**
     * @var string|null - address in non-strict format
     */
    protected $address;
    /**
     * @var string|null
     */
    protected $city;
    /**
     * @var string|null
     */
    protected $region;
    /**
     * @var string|null
     */
    protected $country;
    /**
     * @var float|null - for coord square
     */
    protected $min_lng;
    /**
     * @var float|null - for coord square
     */
    protected $max_lng;
    /**
     * @var float|null - for coord square
     */
    protected $min_lat;
    /**
     * @var float|null - for coord square
     */
    protected $max_lat;
    /**
     * @var string - Y/N 'Y' - if you want to get result for all region, when there is non result for city
     */
    protected $region_cities;
    /**
     * @var mixed|null - when GET-parameter insales is present, server will try to get other request-info from POST fields, expecting Insales-formatted date there
     */
    protected $insales;

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return GetDeliveryInfoModePrice
     */
    public function setId(string $id): GetDeliveryInfoModePrice
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int|null $weight
     * @return GetDeliveryInfoModePrice
     */
    public function setWeight(?int $weight): GetDeliveryInfoModePrice
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }

    /**
     * @param float|null $length
     * @return GetDeliveryInfoModePrice
     */
    public function setLength(?float $length): GetDeliveryInfoModePrice
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @param float|null $width
     * @return GetDeliveryInfoModePrice
     */
    public function setWidth(?float $width): GetDeliveryInfoModePrice
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @param float|null $height
     * @return GetDeliveryInfoModePrice
     */
    public function setHeight(?float $height): GetDeliveryInfoModePrice
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getOrderPrice(): ?float
    {
        return $this->order_price;
    }

    /**
     * @param float|null $order_price
     * @return GetDeliveryInfoModePrice
     */
    public function setOrderPrice(?float $order_price): GetDeliveryInfoModePrice
    {
        $this->order_price = $order_price;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return GetDeliveryInfoModePrice
     */
    public function setType(?string $type): GetDeliveryInfoModePrice
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPartner(): ?string
    {
        return $this->partner;
    }

    /**
     * @param string|null $partner
     * @return GetDeliveryInfoModePrice
     */
    public function setPartner(?string $partner): GetDeliveryInfoModePrice
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFromCity(): ?string
    {
        return $this->fromCity;
    }

    /**
     * @param string|null $fromCity
     * @return GetDeliveryInfoModePrice
     */
    public function setFromCity(?string $fromCity): GetDeliveryInfoModePrice
    {
        $this->fromCity = $fromCity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return GetDeliveryInfoModePrice
     */
    public function setAddress(?string $address): GetDeliveryInfoModePrice
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return GetDeliveryInfoModePrice
     */
    public function setCity(?string $city): GetDeliveryInfoModePrice
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     * @return GetDeliveryInfoModePrice
     */
    public function setRegion(?string $region): GetDeliveryInfoModePrice
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return GetDeliveryInfoModePrice
     */
    public function setCountry(?string $country): GetDeliveryInfoModePrice
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLng(): ?float
    {
        return $this->min_lng;
    }

    /**
     * @param float|null $min_lng
     * @return GetDeliveryInfoModePrice
     */
    public function setMinLng(?float $min_lng): GetDeliveryInfoModePrice
    {
        $this->min_lng = $min_lng;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLng(): ?float
    {
        return $this->max_lng;
    }

    /**
     * @param float|null $max_lng
     * @return GetDeliveryInfoModePrice
     */
    public function setMaxLng(?float $max_lng): GetDeliveryInfoModePrice
    {
        $this->max_lng = $max_lng;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLat(): ?float
    {
        return $this->min_lat;
    }

    /**
     * @param float|null $min_lat
     * @return GetDeliveryInfoModePrice
     */
    public function setMinLat(?float $min_lat): GetDeliveryInfoModePrice
    {
        $this->min_lat = $min_lat;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLat(): ?float
    {
        return $this->max_lat;
    }

    /**
     * @param float|null $max_lat
     * @return GetDeliveryInfoModePrice
     */
    public function setMaxLat(?float $max_lat): GetDeliveryInfoModePrice
    {
        $this->max_lat = $max_lat;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegionCities(): string
    {
        return $this->region_cities;
    }

    /**
     * @param string $region_cities
     * @return GetDeliveryInfoModePrice
     */
    public function setRegionCities(string $region_cities): GetDeliveryInfoModePrice
    {
        $this->region_cities = $region_cities;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getInsales()
    {
        return $this->insales;
    }

    /**
     * @param mixed|null $insales
     * @return GetDeliveryInfoModePrice
     */
    public function setInsales($insales)
    {
        $this->insales = $insales;
        return $this;
    }

}