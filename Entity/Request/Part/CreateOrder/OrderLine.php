<?php


namespace Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder;


use Ipol\Viadelivery\Api\Entity\AbstractEntity;

/**
 * Class OrderLine
 * @package Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder
 */
class OrderLine extends AbstractEntity
{
    /**
     * @var int - -1 for no VAT (0 for 0% VAT is not the same)
     */
    protected $vat;
    /**
     * @var string - product name
     */
    protected $title;
    /**
     * @var float|null - kg (0.1 by default)
     */
    protected $weight;
    /**
     * @var int
     */
    protected $quantity;
    /**
     * @var string|null - format 20x20x10 cm (and 20x20x10 by default)
     */
    protected $dimensions;
    /**
     * @var float - final price for all items in line (itemPrice*quantity|discount)
     */
    protected $full_total_price;
    /**
     * @var - final price for one item of current line (itemPrice|discount)
     */
    protected $full_sale_price;
    /**
     * @deprecated
     * @var - price for all items in line without discount (itemPrice*quantity)
     */
    protected $total_price;
    /**
     * @var integer
     */
    protected $product_id;
    /**
     * @var string|null
     */
    protected $barcode;

    /**
     * @return int
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param int $vat
     * @return OrderLine
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return OrderLine
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     * @return OrderLine
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return OrderLine
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param string|null $dimensions
     * @return OrderLine
     */
    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;
        return $this;
    }

    /**
     * @return float
     */
    public function getFullTotalPrice()
    {
        return $this->full_total_price;
    }

    /**
     * @param float $full_total_price
     * @return OrderLine
     */
    public function setFullTotalPrice($full_total_price)
    {
        $this->full_total_price = $full_total_price;
        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     * @return OrderLine
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string|null $barcode
     * @return OrderLine
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullSalePrice()
    {
        return $this->full_sale_price;
    }

    /**
     * @param mixed $full_sale_price
     * @return OrderLine
     */
    public function setFullSalePrice($full_sale_price)
    {
        $this->full_sale_price = $full_sale_price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * @param mixed $total_price
     * @return OrderLine
     */
    public function setTotalPrice($total_price)
    {
        $this->total_price = $total_price;
        return $this;
    }

}