<?php


namespace Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder;


use Ipol\Viadelivery\Api\Entity\AbstractEntity;

/**
 * Class DeliveryInfo
 * @package Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder
 */
class DeliveryInfo extends AbstractEntity
{
    /**
     * @var string - by default should be "Via.Delivery"
     */
    protected $shipping_company_handle;
    /**
     * @var float
     */
    protected $price;
    /**
     * @var array by default should be []
     */
    protected $errors;
    /**
     * @var array - ['external_id' => $outlet] where $outlet is point uuid
     */
    protected $outlet;

    /**
     * @return string
     */
    public function getShippingCompanyHandle()
    {
        return $this->shipping_company_handle;
    }

    /**
     * @param string $shipping_company_handle
     * @return DeliveryInfo
     */
    public function setShippingCompanyHandle($shipping_company_handle)
    {
        $this->shipping_company_handle = $shipping_company_handle;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return DeliveryInfo
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     * @return DeliveryInfo
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return array
     */
    public function getOutlet()
    {
        return $this->outlet;
    }

    /**
     * @param string $outlet
     * @return DeliveryInfo
     */
    public function setOutlet($outlet)
    {
        $this->outlet = ['external_id' => $outlet];
        return $this;
    }


}