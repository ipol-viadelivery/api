<?php

namespace Ipol\Viadelivery\Api\Entity\Request;


use Ipol\Viadelivery\Api\Entity\AbstractEntity;

/**
 * Class CreateOrder
 * @package Ipol\Viadelivery\Api
 * @subpackage Request
 */
class AbstractRequest extends AbstractEntity
{
    /**
     * @var string|null
     */
    protected $cmsVersion;
    /**
     * @var string|null
     */
    protected $moduleVersion;

    /**
     * @param string|null $cmsVersion
     * @return AbstractRequest
     */
    public function setCmsVersion(?string $cmsVersion): AbstractRequest
    {
        $this->cmsVersion = $cmsVersion;

        return $this;
    }

    /**
     * @param string|null $moduleVersion
     * @return AbstractRequest
     */
    public function setModuleVersion(?string $moduleVersion): AbstractRequest
    {
        $this->moduleVersion = $moduleVersion;

        return $this;
    }
}