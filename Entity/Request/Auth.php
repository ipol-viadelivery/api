<?php

namespace Ipol\Viadelivery\Api\Entity\Request;


/**
 * Class Auth
 * @package Ipol\Viadelivery\API\Entity\Request
 * 
 * @var string $uid
 * @var string $token
 */

class Auth extends AbstractRequest
{
    protected $uid;
    protected $token;

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return Auth
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return Auth
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

   
}