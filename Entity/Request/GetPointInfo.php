<?php


namespace Ipol\Viadelivery\Api\Entity\Request;


/**
 * Class GetPointInfo
 * @package Ipol\Viadelivery\Api
 * @subpackage Request
 */
class GetPointInfo extends AbstractRequest
{
    /**
     * @var string - shop UUID - documentations marks this optional, but really now its required
     */
    protected $id;
    /**
     * @var string - UUID of pickup point
     */
    protected $point_id;
    /**
     * @var int|null gram
     */
    protected $weight;
    /**
     * @var float|null cm
     */
    protected $length;
    /**
     * @var float|null cm
     */
    protected $width;
    /**
     * @var float|null cm
     */
    protected $height;
    /**
     * @var float|null
     */
    protected $order_price;
    /**
     * @var string|null
     */
    protected $city;
    /**
     * @var string|null
     */
    protected $region;
    /**
     * @var string|null
     */
    protected $country;
    /**
     * @var string|null - POSTAMAT|TOBACCO|RESTAURANT|POINT
     */
    protected $type;
    /**
     * @var string|null - X5|Sberlogistics
     */
    protected $partner;
    /**
     * @var float|null - for coord square
     */
    protected $min_lng;
    /**
     * @var float|null - for coord square
     */
    protected $max_lng;
    /**
     * @var float|null - for coord square
     */
    protected $min_lat;
    /**
     * @var float|null - for coord square
     */
    protected $max_lat;
    /**
     * @var string - Y/N 'Y' - if you want to get result for all region, when there is non result for city
     */
    protected $region_cities;
    /**
     * @var mixed|null - when GET-parameter insales is present, server will try to get other request-info from POST fields, expecting Insales-formatted date there
     */
    protected $insales;
    /**
     * @var string|null
     */
    protected $address;
    /**
     * @var string|null
     */
    protected $zip_code;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return GetPointInfo
     */
    public function setId(string $id): GetPointInfo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPointId(): string
    {
        return $this->point_id;
    }

    /**
     * @param string $point_id
     * @return GetPointInfo
     */
    public function setPointId(string $point_id): GetPointInfo
    {
        $this->point_id = $point_id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int|null $weight
     * @return GetPointInfo
     */
    public function setWeight(?int $weight): GetPointInfo
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }

    /**
     * @param float|null $length
     * @return GetPointInfo
     */
    public function setLength(?float $length): GetPointInfo
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @param float|null $width
     * @return GetPointInfo
     */
    public function setWidth(?float $width): GetPointInfo
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @param float|null $height
     * @return GetPointInfo
     */
    public function setHeight(?float $height): GetPointInfo
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getOrderPrice(): ?float
    {
        return $this->order_price;
    }

    /**
     * @param float|null $order_price
     * @return GetPointInfo
     */
    public function setOrderPrice(?float $order_price): GetPointInfo
    {
        $this->order_price = $order_price;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return GetPointInfo
     */
    public function setCity(?string $city): GetPointInfo
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     * @return GetPointInfo
     */
    public function setRegion(?string $region): GetPointInfo
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return GetPointInfo
     */
    public function setCountry(?string $country): GetPointInfo
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return GetPointInfo
     */
    public function setType(?string $type): GetPointInfo
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPartner(): ?string
    {
        return $this->partner;
    }

    /**
     * @param string|null $partner
     * @return GetPointInfo
     */
    public function setPartner(?string $partner): GetPointInfo
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLng(): ?float
    {
        return $this->min_lng;
    }

    /**
     * @param float|null $min_lng
     * @return GetPointInfo
     */
    public function setMinLng(?float $min_lng): GetPointInfo
    {
        $this->min_lng = $min_lng;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLng(): ?float
    {
        return $this->max_lng;
    }

    /**
     * @param float|null $max_lng
     * @return GetPointInfo
     */
    public function setMaxLng(?float $max_lng): GetPointInfo
    {
        $this->max_lng = $max_lng;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLat(): ?float
    {
        return $this->min_lat;
    }

    /**
     * @param float|null $min_lat
     * @return GetPointInfo
     */
    public function setMinLat(?float $min_lat): GetPointInfo
    {
        $this->min_lat = $min_lat;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLat(): ?float
    {
        return $this->max_lat;
    }

    /**
     * @param float|null $max_lat
     * @return GetPointInfo
     */
    public function setMaxLat(?float $max_lat): GetPointInfo
    {
        $this->max_lat = $max_lat;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegionCities(): string
    {
        return $this->region_cities;
    }

    /**
     * @param string $region_cities
     * @return GetPointInfo
     */
    public function setRegionCities(string $region_cities): GetPointInfo
    {
        $this->region_cities = $region_cities;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getInsales()
    {
        return $this->insales;
    }

    /**
     * @param mixed|null $insales
     * @return GetPointInfo
     */
    public function setInsales($insales)
    {
        $this->insales = $insales;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return GetPointInfo
     */
    public function setAddress(?string $address): GetPointInfo
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    /**
     * @param string|null $zip_code
     * @return GetPointInfo
     */
    public function setZipCode(?string $zip_code): GetPointInfo
    {
        $this->zip_code = $zip_code;
        return $this;
    }

}