<?php


namespace Ipol\Viadelivery\Api\Entity\Request;


/**
 * Class GetDeliveryInfo
 * @package Ipol\Viadelivery\Api
 * @subpackage Request
 */
class GetDeliveryInfoModeDistance extends AbstractRequest
{
    /**
     * @var string = distance - mode for closest points
     */
    protected $group = 'distance';
    /**
     * @var string - shop UUID - documentations marks this optional, but really now its required
     */
    protected $id = 'default';
    /**
     * @var int|null gram
     */
    protected $weight;
    /**
     * @var float|null cm
     */
    protected $length;
    /**
     * @var float|null cm
     */
    protected $width;
    /**
     * @var float|null cm
     */
    protected $height;
    /**
     * @var float|null
     */
    protected $order_price;
    /**
     * @var string|null - POSTAMAT|TOBACCO|RESTAURANT|POINT
     */
    protected $type;
    /**
     * @var string|null - X5|Sberlogistics
     */
    protected $partner;
    /*------parameters that technically are allowed, but using them makes no sense------*/
    /**
     * @var string|null
     */
    protected $city;
    /**
     * @var string|null
     */
    protected $region;
    /**
     * @var string|null
     */
    protected $country;
    /**
     * @var float|null - for coord square
     */
    protected $min_lng;
    /**
     * @var float|null - for coord square
     */
    protected $max_lng;
    /**
     * @var float|null - for coord square
     */
    protected $min_lat;
    /**
     * @var float|null - for coord square
     */
    protected $max_lat;
    /**
     * @var string - Y/N 'Y' - if you want to get result for all region, when there is non result for city
     */
    protected $region_cities;
    /*---------en of silly parameters-----------*/
    /**
     * @var mixed|null - when GET-parameter insales is present, server will try to get other request-info from POST fields, expecting Insales-formatted date there
     */
    protected $insales;
    /**
     * @var int|null - how many closest points to return (10 by default)
     */
    protected $group_limit;
    /**
     * @var string - address in non-strict format
     */
    protected $address;

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return GetDeliveryInfoModeDistance
     */
    public function setId(string $id): GetDeliveryInfoModeDistance
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int|null $weight
     * @return GetDeliveryInfoModeDistance
     */
    public function setWeight(?int $weight): GetDeliveryInfoModeDistance
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }

    /**
     * @param float|null $length
     * @return GetDeliveryInfoModeDistance
     */
    public function setLength(?float $length): GetDeliveryInfoModeDistance
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @param float|null $width
     * @return GetDeliveryInfoModeDistance
     */
    public function setWidth(?float $width): GetDeliveryInfoModeDistance
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @param float|null $height
     * @return GetDeliveryInfoModeDistance
     */
    public function setHeight(?float $height): GetDeliveryInfoModeDistance
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getOrderPrice(): ?float
    {
        return $this->order_price;
    }

    /**
     * @param float|null $order_price
     * @return GetDeliveryInfoModeDistance
     */
    public function setOrderPrice(?float $order_price): GetDeliveryInfoModeDistance
    {
        $this->order_price = $order_price;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return GetDeliveryInfoModeDistance
     */
    public function setCity(?string $city): GetDeliveryInfoModeDistance
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     * @return GetDeliveryInfoModeDistance
     */
    public function setRegion(?string $region): GetDeliveryInfoModeDistance
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return GetDeliveryInfoModeDistance
     */
    public function setCountry(?string $country): GetDeliveryInfoModeDistance
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return GetDeliveryInfoModeDistance
     */
    public function setType(?string $type): GetDeliveryInfoModeDistance
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPartner(): ?string
    {
        return $this->partner;
    }

    /**
     * @param string|null $partner
     * @return GetDeliveryInfoModeDistance
     */
    public function setPartner(?string $partner): GetDeliveryInfoModeDistance
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLng(): ?float
    {
        return $this->min_lng;
    }

    /**
     * @param float|null $min_lng
     * @return GetDeliveryInfoModeDistance
     */
    public function setMinLng(?float $min_lng): GetDeliveryInfoModeDistance
    {
        $this->min_lng = $min_lng;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLng(): ?float
    {
        return $this->max_lng;
    }

    /**
     * @param float|null $max_lng
     * @return GetDeliveryInfoModeDistance
     */
    public function setMaxLng(?float $max_lng): GetDeliveryInfoModeDistance
    {
        $this->max_lng = $max_lng;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLat(): ?float
    {
        return $this->min_lat;
    }

    /**
     * @param float|null $min_lat
     * @return GetDeliveryInfoModeDistance
     */
    public function setMinLat(?float $min_lat): GetDeliveryInfoModeDistance
    {
        $this->min_lat = $min_lat;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLat(): ?float
    {
        return $this->max_lat;
    }

    /**
     * @param float|null $max_lat
     * @return GetDeliveryInfoModeDistance
     */
    public function setMaxLat(?float $max_lat): GetDeliveryInfoModeDistance
    {
        $this->max_lat = $max_lat;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegionCities(): string
    {
        return $this->region_cities;
    }

    /**
     * @param string $region_cities
     * @return GetDeliveryInfoModeDistance
     */
    public function setRegionCities(?string $region_cities): GetDeliveryInfoModeDistance
    {
        $this->region_cities = $region_cities;

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getInsales()
    {
        return $this->insales;
    }

    /**
     * @param mixed|null $insales
     * @return GetDeliveryInfoModeDistance
     */
    public function setInsales($insales)
    {
        $this->insales = $insales;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getGroupLimit(): ?int
    {
        return $this->group_limit;
    }

    /**
     * @param int|null $group_limit
     * @return GetDeliveryInfoModeDistance
     */
    public function setGroupLimit(?int $group_limit): GetDeliveryInfoModeDistance
    {
        $this->group_limit = $group_limit;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return GetDeliveryInfoModeDistance
     */
    public function setAddress(string $address): GetDeliveryInfoModeDistance
    {
        $this->address = $address;
        return $this;
    }

}