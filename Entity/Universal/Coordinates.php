<?php


namespace Ipol\Viadelivery\Api\Entity\Universal;


use Ipol\Viadelivery\Api\Entity\AbstractEntity;

class Coordinates extends AbstractEntity
{
    /**
     * @var float
     */
    protected $lat;
    /**
     * @var float
     */
    protected $lng;

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng)
    {
        $this->lng = $lng;
    }

}