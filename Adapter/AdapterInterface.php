<?php
    namespace Ipol\Viadelivery\Api\Adapter;

    interface AdapterInterface
    {
        public function post($method, $dataPost = array());
    }
?>