<?php


namespace Ipol\Viadelivery\Api;


use Exception;

/**
 * Class BadResponseException
 * @package Ipol\Viadelivery\Api
 */
class BadResponseException extends Exception
{

}