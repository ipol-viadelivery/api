<?php


namespace Ipol\Viadelivery\Api\Methods;


use Ipol\Viadelivery\Api\Adapter\CurlAdapter;

/**
 * Class GetFullMapLink
 * @package Ipol\Viadelivery\Api
 * @subpackage Methods
 * @method string getResponse
 */
class GetFullMapLink extends AbstractMethod
{
    public function __construct($data, CurlAdapter $adapter, $encoder = false)
    {
        parent::__construct($adapter, $encoder);

        $arWdgtParams = $this->getEntityFields($data);
        $wdgtParamsStr = http_build_query($arWdgtParams);
        $wdgtParamsStr = $this->encodeFieldToAPI($wdgtParamsStr);
        $this->setResponse($this->adapter->getUrl() . '/?' . $wdgtParamsStr);

        $this->adapter->getLog()->debug('', [
            'method' => $this->method,
            'process' => 'MAP LINK',
            'content' => ['URL' => $this->getResponse()],
        ]);

        return $this;
    }
}