<?php


namespace Ipol\Viadelivery\Api\Logger;


/**
 * Class FileRoute
 * @package Ipol\Viadelivery\Api
 * @subpackage Logger
 */
class InlineRoute extends Route
{
    /**
     * @param string $dataString
     */
    public function log(string $dataString): void
    {
        echo $dataString . PHP_EOL . '--------------------------------------------' . PHP_EOL;
    }

}