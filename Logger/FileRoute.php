<?php


namespace Ipol\Viadelivery\Api\Logger;


/**
 * Class FileRoute
 * @package Ipol\Viadelivery\Api
 * @subpackage Logger
 */
class FileRoute extends Route
{
    /**
     * @var string ���� � �����
     */
    public $filePath;

    /**
     * FileRoute constructor.
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @param string $dataString
     */
    public function log(string $dataString): void
    {
        if(!file_exists($this->filePath)) {
            $dirPath = substr($this->filePath, 0, strrpos($this->filePath, '/', -1));
            mkdir($dirPath, 0755, true);
        }

        file_put_contents($this->filePath,
            trim($dataString) . PHP_EOL . '--------------------------------------------' . PHP_EOL,
            FILE_APPEND);
    }
}